﻿/* script para criacao do schema e users para OWGFB - HOM */


set serveroutput on 
set define off
set echo on 
spool owGFB_hom_30062017




create bigfile tablespace TS_OWGFB_DATA datafile '+DATA/hauto/datafile/TS_OWGFB_DATA01.dbf' SIZE 10M REUSE  AUTOEXTEND ON NEXT 5M MAXSIZE 100M
/


 
create user  USGFB identified by Alelo_2017 default tablespace TS_OWGFB_DATA  
/

create user  OWGFB identified by null default tablespace TS_OWGFB_DATA 
/


grant create session, unlimited tablespace to USGFB, OWGFB 
/


grant resource to OWGFB
/



create role RL_OWGFB_RW; 
create role RL_OWGFB_RO; 


grant RL_OWGFB_RW to USGFB,OWGFB;



/* Executa scripts de criacao dos objetos do fornecedor */ 

set sqlblanklines on 
/
alter session set current_schema = OWGFB
/



/* especifica as Roles para os usuarios */ 





/* script para criacao da trigger de logon */ 


CREATE OR REPLACE TRIGGER usgfb.after_logon_trg
AFTER LOGON ON usgfb.SCHEMA
BEGIN
  DBMS_APPLICATION_INFO.set_module(USER, 'Initialized');
  EXECUTE IMMEDIATE 'ALTER SESSION SET current_schema=OWGFB';
END;
/



spool off
/


