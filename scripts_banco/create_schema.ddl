-- =========================================================
-- verificar os tablespaces
SELECT
    b.tablespace_name,
    tbs_size sizemb,
    a.free_space freemb
FROM
    (
        SELECT
            tablespace_name,
            round(
                SUM(bytes) / 1024 / 1024,
                2
            ) AS free_space
        FROM
            dba_free_space
        GROUP BY
            tablespace_name
    ) a,
    (
        SELECT
            tablespace_name,
            SUM(bytes) / 1024 / 1024 AS tbs_size
        FROM
            dba_data_files
        GROUP BY
            tablespace_name
    ) b
WHERE
    a.tablespace_name (+) = b.tablespace_name;


-- ===============================================
-- criação de table space
-- The following is a CREATE TABLESPACE statement that creates a permanent tablespace that will extend when more space is required

CREATE TABLESPACE fleet_desenv DATAFILE SIZE 100M EXTENT MANAGEMENT LOCAL AUTOALLOCATE;


-- ===============================================
-- criação de usuário
CREATE USER fleet_desenv IDENTIFIED BY fleet_desenv DEFAULT TABLESPACE fleet_desenv;


-- ===============================================
-- criação de grants
GRANT connect, resource TO fleet_desenv;





