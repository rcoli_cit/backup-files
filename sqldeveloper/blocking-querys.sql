-- ver o sid da query que está bloqueando as outras
select 
  (select username from v$session where sid=a.sid) blocker,
  a.sid,
  ' is blocking ',
  (select username from v$session where sid=b.sid) blockee,
  b.sid
from 
  v$lock a, 
  v$lock b
where 
  a.block = 1
and 
  b.request > 0
and 
  a.id1 = b.id1
and 
  a.id2 = b.id2;
 
 
-- vc tem mais algumas informações inclusive de qual máquina a conexão está aberta  
select
  c.owner,
  c.object_name,
  c.object_type,
  b.sid,
  b.serial#,
  b.status,
  b.osuser,
  b.machine  
from
  v$locked_object a ,
  v$session b,
  dba_objects c
where
  b.sid = a.session_id
and
  a.object_id = c.object_id;