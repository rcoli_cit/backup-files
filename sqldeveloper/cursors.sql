-- MUST BE SYSDBA
-- --------------------------------
-- Seleciona os cursores por usuario
-- --------------------------------
SELECT a.value AS open_cursors,
    s.username,
    s.sid,
    s.serial#
  FROM v$sesstat a,
    v$statname b,
    v$session s
  WHERE a.statistic# = b.statistic#
  AND s.sid          =a.sid
  AND b.name         = 'opened cursors current'
  AND s.username    IS NOT NULL
  AND S.USERNAME like 'PEDIDO_TESTE'
  ORDER BY open_cursors DESC;
  
-- OU 

select sql_text, USER_NAME,  count(sql_text) from v$open_cursor WHERE USER_NAME = 'PEDIDO_TESTE' group by sql_text, user_name;
 
-- -------------------------------------------
-- EXIBE A QUERY MAIS CUSTOSA DO USUARIO COM MAIOR NUMERO DE CURSORES
-- ------------------------------------------
SELECT sid ,
  sql_text,
  COUNT(*) AS open_cursors,
  USER_NAME
FROM v$open_cursor
WHERE sid IN
  (SELECT SID
  FROM
    (SELECT a.value AS open_cursors,
      s.username,
      s.sid,
      s.serial#
    FROM v$sesstat a,
      v$statname b,
      v$session s
    WHERE a.statistic# = b.statistic#
    AND s.sid          =a.sid
    AND b.name         = 'opened cursors current'
    AND s.username    IS NOT NULL
    ORDER BY open_cursors DESC
    )
  WHERE ROWNUM <= 1
  )
GROUP BY sid ,
  sql_text,
  USER_NAME
ORDER BY open_cursors DESC
;
--OU

SELECT sid ,
  sql_text,
  COUNT(*) AS open_cursors,
  USER_NAME
FROM v$open_cursor
WHERE sid IN (128)
GROUP BY sid ,
  sql_text,
  USER_NAME
ORDER BY open_cursors DESC;

-- ---------------------------------------------------------
-- To verify if you have set the value of the OPEN_CURSORS parameter high enough,
-- monitor v$sesstat for the maximum opened cursors current, as shown
-- ---------------------------------------------------------
SELECT MAX(a.value) AS highest_open_cur,
  p.value           AS max_open_cur
FROM v$sesstat a,
  v$statname b,
  v$parameter p
WHERE a.statistic# = b.statistic#
AND b.name         = 'opened cursors current'
AND p.name         = 'open_cursors'
GROUP BY p.value;
-- --------------------------------------------------------------
-- MATANDO A CONEXAO 
-- --------------------------------------------------------------
-- ALTER SYSTEM DISCONNECT SESSION 'sid,serial#' POST_TRANSACTION
-- ALTER SYSTEM KILL SESSION '143,4793' IMMEDIATE;

