== Atalhos mais usados

CTRL       + SHIFT + D       - Duplicate line
CTRL       + SHIFT + K       - Remove line
CTRL               + /       - Comment
CTRL       + SHIFT + /       - Uncomment
CTRL       + SHIFT + UP/DOWN - Move Line Up or Down
Selection  +  ALT  +  F3     - Replace all ocorrences
