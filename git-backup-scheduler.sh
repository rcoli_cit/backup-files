#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DATE="$(/bin/date)"
export GIT_SSH_COMMAND="ssh -i $DIR/.ssh/bitbucket-rcoli-key"
cd $DIR

echo "## $DATE"
echo "## Exporting GIT_SSH_COMMAND : $GIT_SSH_COMMAND"
echo "## cd to directory: $(pwd)"

git add --all
git commit -m "Scheduled Backup [$DATE]"
git push origin master


# Example: Run backup cron job script
# # crontab -e
#  
# Append the following entry:
# @daily ~/backup-files/git-backup-scheduler.sh >> /tmp/cron_git.log