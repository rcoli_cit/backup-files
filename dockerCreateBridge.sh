# Create our own bridge

sudo brctl addbr bridge0
sudo ip addr add 192.168.5.1/24 dev bridge0
sudo ip link set dev bridge0 up

# Confirming that our bridge is up and running

ip addr show bridge0

#4: bridge0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state UP group default
#    link/ether 66:38:d0:0d:76:18 brd ff:ff:ff:ff:ff:ff
#    inet 192.168.5.1/24 scope global bridge0
#       valid_lft forever preferred_lft forever

# Tell Docker about it and restart (on Ubuntu)

sudo echo 'DOCKER_OPTS="-b=bridge0"' >> /etc/default/docker
sudo service docker start

# Confirming new outgoing NAT masquerade is set up

sudo iptables -t nat -L -n
#...
#hain POSTROUTING (policy ACCEPT)
#target     prot opt source               destination
#MASQUERADE  all  --  192.168.5.0/24      0.0.0.0/0
