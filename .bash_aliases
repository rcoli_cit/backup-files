# Alelo Repos
alias rac='cd ~/alelo/alelo_repository/alelo_commons && git fetch && git status'
alias rgeral='cd ~/alelo/alelo_repository/alelo_geral && git fetch && git status'
alias rma='cd ~/alelo/alelo_repository/alelo_meualelo && git fetch && git status'
alias radmin='cd ~/alelo/alelo_repository/alelo_admin && git fetch && git status'
alias rpedido='cd ~/alelo/alelo_repository/alelo_pedido && git fetch && git status'
alias rscaapi='cd ~/alelo/alelo_repository/sca_api && git fetch && git status'
alias rscadb='cd ~/alelo/alelo_repository/sca_database && git fetch && git status'
alias rscaapp='cd ~/alelo/alelo_repository/sca_app && git fetch && git status'
alias rauto='cd ~/alelo_repo/alelo_auto && git fetch && git status'
alias rwcm='cd ~/alelo/alelo_repository/alelo_meualelo_frontend && git fetch && git status'
alias rir='cd ~/alelo/alelo_repository/alelo_informe_rendimentos && git fetch && git status'
alias rsts='cd ~/alelo/alelo_repository/alelo_sts && git fetch && git status'
alias rsca='rscadb,rscaapi,rscaapp'
alias ralelo='rcommonsalelo;rgeral;rma;radmin;rpedido'
alias rconfig='cd ~/alelo/alelo_repository/alelo_config && git fetch && git status'
alias rdevice="cd ~/alelo/alelo_repository/alelo_device && git fetch && git status"
alias rfleet="cd ~/alelo/alelo_repository/alelo_fleet && git fetch && git status"
alias rpi="cd ~/alelo/alelo_repository/alelo_pi && git fetch && git status"
alias rlower="cd ~/alelo/alelo_repository/alelo-docker-lower-environments-conf && git fetch && git status"
alias rlogger="cd ~/alelo/alelo_repository/alelo-commons-logging && git fetch && git status"
alias rpatch="cd ~/alelo/alelo_repository/alelo_patch_control && git fetch && git status"

# ALelo Servers
alias wcmp1='sshpass -p S3nh@1234 ssh denisg@10.150.91.172 && bash'
alias wcmp2='sshpass -p S3nh@1234 ssh denisg@10.150.91.173 && bash'
alias uat1='sshpass -p Rm@rQ#3Z ssh rmarquez@10.151.14.9 && bash'
alias uat2='sshpass -p Senh@010 ssh natanael@10.151.14.101 && bash'
alias prd1='sshpass -p Senh@010 ssh natanael@10.150.91.115 && bash'
alias prd2='sshpass -p Senh@010 ssh natanael@10.150.91.116 && bash'
alias leon='ssh rede@leon.cit -i ~/.ssh/id_dsa_rede'
alias alipio='ssh -X ec2-user@alipio.cit -i ~/backup-files/ALELO-NEW.pem'
alias alicia='ssh -X ec2-user@alicia.cit -i ~/backup-files/ALELO-NEW.pem'
alias lamia='ssh -X ec2-user@lamia.cit -i ~/backup-files/ALELO-NEW.pem'
alias stark='ssh -X ec2-user@alelo-stark.cit -i ~/backup-files/ALELO-NEW.pem'
alias anor='ssh -X ec2-user@anor.cit -i ~/backup-files/ALELO-NEW.pem'
alias sadala='ssh -X ec2-user@sadala.cit -i ~/backup-files/ALELO-NEW.pem'
alias sasha='ssh -X ec2-user@sasha.cit -i ~/backup-files/ALELO-NEW.pem'
alias saint='ssh -X ec2-user@saint.cit -i ~/backup-files/ALELO-NEW.pem'
alias champap='ssh centos@champa-publisher.cit -i ~/backup-files/ALELO-NEW.pem'
alias champaa='ssh centos@champa-author.cit -i ~/backup-files/ALELO-NEW.pem'
alias macincloud='sshpass -p fspxx6dx9xe ssh admin@dn147.macincloud.com '

# Other Stuff
alias tuat='ssh -lciandt -L 1521:10.40.89.36:1521 10.150.91.101 && Alelo@2015'
alias jenkinsrestart='sudo java -jar /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar -s http://localhost:8080/ safe-restart'
alias starteam='/home/CIT/rcoli/opt/starteam-en-14.0.4-java/bin/StarteamCPC'
alias rcoliAws='sudo ssh ec2-user@ec2-52-37-5-54.us-west-2.compute.amazonaws.com -i ~/.ssh/id_dsa_aws_rcoli'
alias deployma='rma && clear && ferramentas/deploy-ma-was-lamia.sh ~/alelo/alelo_repository/alelo_meualelo local $WAS_HOME cit011459-UbuntuNode01'
alias stopwas='$WAS_HOME/stopServer.sh'
alias startwas='$WAS_HOME/starServer.sh'
alias java8='/usr/lib/jvm/java-8-oracle/bin/java'
